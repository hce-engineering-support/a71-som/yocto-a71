From 824396ea2726b09a10626918f1b24e346d0118b2 Mon Sep 17 00:00:00 2001
From: vasujois <vasujois@gmail.com>
Date: Fri, 8 Mar 2024 15:35:36 +0530
Subject: [PATCH] Added fec and video support in u-boot

Signed-off-by: vasujois <vasujois@gmail.com>
---
 board/freescale/mx7da71/mx7da71.c | 144 +++++++++++++++++++++++++++++-
 include/configs/mx7da71.h         |  28 ++++++
 2 files changed, 171 insertions(+), 1 deletion(-)

diff --git a/board/freescale/mx7da71/mx7da71.c b/board/freescale/mx7da71/mx7da71.c
index cb83f83574..3717eb26bb 100644
--- a/board/freescale/mx7da71/mx7da71.c
+++ b/board/freescale/mx7da71/mx7da71.c
@@ -32,6 +32,14 @@ DECLARE_GLOBAL_DATA_PTR;
 	PAD_CTL_HYS | PAD_CTL_PUE | PAD_CTL_PUS_PU47KOHM)
 #define SPI_PAD_CTRL (PAD_CTL_DSE_3P3V_49OHM | PAD_CTL_SRE_SLOW | PAD_CTL_HYS)
 
+#define ENET_PAD_CTRL  (PAD_CTL_PUS_PU100KOHM | PAD_CTL_DSE_3P3V_49OHM)
+#define ENET_PAD_CTRL_MII  (PAD_CTL_DSE_3P3V_32OHM)
+
+#define ENET_RX_PAD_CTRL  (PAD_CTL_PUS_PU100KOHM | PAD_CTL_DSE_3P3V_49OHM)
+
+#define LCD_PAD_CTRL    (PAD_CTL_HYS | PAD_CTL_PUS_PU100KOHM | \
+	PAD_CTL_DSE_3P3V_49OHM)
+
 int dram_init(void)
 {
 	gd->ram_size = PHYS_SDRAM_SIZE;
@@ -53,6 +61,83 @@ static void setup_iomux_uart(void)
 	imx_iomux_v3_setup_multiple_pads(uart1_pads, ARRAY_SIZE(uart1_pads));
 }
 
+#ifdef CONFIG_VIDEO_MXS
+static iomux_v3_cfg_t const lcd_pads[] = {
+        MX7D_PAD_LCD_CLK__LCD_CLK | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_ENABLE__LCD_ENABLE | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_HSYNC__LCD_HSYNC | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_VSYNC__LCD_VSYNC | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA00__LCD_DATA0 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA01__LCD_DATA1 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA02__LCD_DATA2 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA03__LCD_DATA3 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA04__LCD_DATA4 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA05__LCD_DATA5 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA06__LCD_DATA6 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA07__LCD_DATA7 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA08__LCD_DATA8 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA09__LCD_DATA9 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA10__LCD_DATA10 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA11__LCD_DATA11 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA12__LCD_DATA12 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA13__LCD_DATA13 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA14__LCD_DATA14 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA15__LCD_DATA15 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA16__LCD_DATA16 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA17__LCD_DATA17 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA18__LCD_DATA18 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA19__LCD_DATA19 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA20__LCD_DATA20 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA21__LCD_DATA21 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA22__LCD_DATA22 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+        MX7D_PAD_LCD_DATA23__LCD_DATA23 | MUX_PAD_CTRL(LCD_PAD_CTRL),
+
+        MX7D_PAD_LCD_RESET__GPIO3_IO4   | MUX_PAD_CTRL(LCD_PAD_CTRL),
+};
+
+static iomux_v3_cfg_t const pwm_pads[] = {
+        /* Use GPIO for Brightness adjustment, duty cycle = period */
+        MX7D_PAD_GPIO1_IO01__GPIO1_IO1 | MUX_PAD_CTRL(NO_PAD_CTRL),
+};
+
+static int setup_lcd(void)
+{
+	imx_iomux_v3_setup_multiple_pads(lcd_pads, ARRAY_SIZE(lcd_pads));
+
+	imx_iomux_v3_setup_multiple_pads(pwm_pads, ARRAY_SIZE(pwm_pads));
+
+	/* Reset LCD */
+	gpio_direction_output(IMX_GPIO_NR(3, 4) , 0);
+	udelay(500);
+	gpio_direction_output(IMX_GPIO_NR(3, 4) , 1);
+
+	/* Set Brightness to high */
+	gpio_direction_output(IMX_GPIO_NR(1, 2) , 1);
+
+	return 0;
+}
+#endif
+
+#ifdef CONFIG_FEC_MXC
+static iomux_v3_cfg_t const fec1_pads[] = {
+	MX7D_PAD_GPIO1_IO14__GPIO1_IO14 | MUX_PAD_CTRL(NO_PAD_CTRL), /* phy1 reset */
+	MX7D_PAD_ENET1_RGMII_TD0__ENET1_RGMII_TD0 | MUX_PAD_CTRL(ENET_PAD_CTRL),
+	MX7D_PAD_ENET1_RGMII_TD1__ENET1_RGMII_TD1 | MUX_PAD_CTRL(ENET_PAD_CTRL),
+	MX7D_PAD_ENET1_RGMII_TX_CTL__ENET1_RGMII_TX_CTL | MUX_PAD_CTRL(ENET_PAD_CTRL),
+	MX7D_PAD_ENET1_RGMII_RD0__ENET1_RGMII_RD0 | MUX_PAD_CTRL(ENET_RX_PAD_CTRL),
+	MX7D_PAD_ENET1_RGMII_RD1__ENET1_RGMII_RD1 | MUX_PAD_CTRL(ENET_RX_PAD_CTRL),
+	MX7D_PAD_ENET1_RGMII_RX_CTL__ENET1_RGMII_RX_CTL | MUX_PAD_CTRL(ENET_RX_PAD_CTRL),
+	MX7D_PAD_ENET1_RGMII_RXC__ENET1_RX_ER | MUX_PAD_CTRL(ENET_RX_PAD_CTRL),
+	MX7D_PAD_ENET1_TX_CLK__CCM_ENET_REF_CLK1 | MUX_PAD_CTRL(ENET_PAD_CTRL),
+	MX7D_PAD_SD2_CD_B__ENET1_MDIO | MUX_PAD_CTRL(ENET_PAD_CTRL_MII),
+	MX7D_PAD_SD2_WP__ENET1_MDC | MUX_PAD_CTRL(ENET_PAD_CTRL_MII),
+};
+
+static void setup_iomux_fec(void)
+{
+	imx_iomux_v3_setup_multiple_pads(fec1_pads, ARRAY_SIZE(fec1_pads));
+}
+#endif
 
 #ifdef CONFIG_MXC_SPI
 #ifndef CONFIG_DM_SPI
@@ -82,6 +167,55 @@ int board_spi_cs_gpio(unsigned bus, unsigned cs)
 #endif
 #endif
 
+#ifdef CONFIG_FEC_MXC
+int board_eth_init(struct bd_info *bis)
+{
+	int ret;
+
+	setup_iomux_fec();
+
+	ret = fecmxc_initialize_multi(bis, 0,
+		CONFIG_FEC_MXC_PHYADDR, IMX_FEC_BASE);
+	if (ret)
+		printf("FEC1 MXC: %s:failed\n", __func__);
+
+	return ret;
+}
+
+static int setup_fec(void)
+{
+	struct iomuxc_gpr_base_regs *const iomuxc_gpr_regs
+		= (struct iomuxc_gpr_base_regs *) IOMUXC_GPR_BASE_ADDR;
+	int ret, phy_rst_gpio;
+
+	/* Use 125M anatop REF_CLK1 for ENET1, clear gpr1[13], gpr1[17]*/
+	clrsetbits_le32(&iomuxc_gpr_regs->gpr[1],
+		(IOMUXC_GPR_GPR1_GPR_ENET1_TX_CLK_SEL_MASK |
+		 IOMUXC_GPR_GPR1_GPR_ENET1_CLK_DIR_MASK), 0);
+
+	ret = set_clk_enet(ENET_125MHZ);
+	if (ret) {
+		printf("FEC1 MXC: %s:failed\n", __func__);
+		return ret;
+	}
+
+	phy_rst_gpio = IMX_GPIO_NR(1, 14);
+	gpio_direction_output(phy_rst_gpio, 0);
+	mdelay(10);
+	gpio_direction_output(phy_rst_gpio, 1);
+
+	return 0;
+}
+
+
+int board_phy_config(struct phy_device *phydev)
+{
+	if (phydev->drv->config)
+		phydev->drv->config(phydev);
+	return 0;
+}
+#endif
+
 int board_early_init_f(void)
 {
 	setup_iomux_uart();
@@ -100,6 +234,14 @@ int board_init(void)
 #endif
 #endif
 
+#ifdef CONFIG_FEC_MXC
+	setup_fec();
+#endif
+
+#ifdef CONFIG_VIDEO_MXS
+	setup_lcd();
+#endif
+
 	return 0;
 }
 
@@ -177,7 +319,7 @@ u32 get_board_rev(void)
 
 int checkboard(void)
 {
-	puts("Board: MX7D 12x12 DDR3 VAL\n");
+	puts("Board: HCE Engineering : MX7D 12x12 DDR3 VAL booting...!!!\n");
 
 	return 0;
 }
diff --git a/include/configs/mx7da71.h b/include/configs/mx7da71.h
index 87facef6ef..47fee6064a 100644
--- a/include/configs/mx7da71.h
+++ b/include/configs/mx7da71.h
@@ -15,6 +15,34 @@
 
 #define PHYS_SDRAM_SIZE			SZ_1G
 
+/* Network */
+#define CONFIG_CMD_MII
+#define CONFIG_FEC_MXC
+#define CONFIG_MII
+#define CONFIG_FEC_XCV_TYPE             RMII
+#define CONFIG_ETHPRIME                 "FEC"
+#define CONFIG_FEC_MXC_PHYADDR          1
+
+#define CONFIG_PHYLIB
+#define CONFIG_PHY_MICREL
+/* ENET1 */
+#define IMX_FEC_BASE			ENET_IPS_BASE_ADDR
+
+//#define CONFIG_VIDEO
+//#ifdef CONFIG_VIDEO
+//#define CONFIG_CFB_CONSOLE
+//#define CONFIG_VIDEO_MXS
+//#define CONFIG_VIDEO_LOGO
+//#define CONFIG_VIDEO_SW_CURSOR
+//#define CONFIG_VGA_AS_SINGLE_DEVICE
+//#define CONFIG_SYS_CONSOLE_IS_IN_ENV
+//#define CONFIG_SPLASH_SCREEN
+//#define CONFIG_SPLASH_SCREEN_ALIGN
+//#define CONFIG_CMD_BMP
+//#define CONFIG_BMP_16BPP
+//#define CONFIG_VIDEO_BMP_RLE8
+//#define CONFIG_VIDEO_BMP_LOGO
+//#endif
 
 #include "mx7d_val.h"
 
-- 
2.17.1

