FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " file://imx7d-hce-a7122-bb536.dts \
		file://imx7d-hce-a71.dtsi \
		file://imx7-hce-a7122-bb536_defconfig \
		file://0001-Added-HCE-logo.patch \
		file://0002-Added-wilc-driver.patch \
		"

do_configure:append () {
        install -d ${D}/arch/arm/boot/dts
        cp ${WORKDIR}/imx7d-hce-a71.dtsi ${S}/arch/arm/boot/dts
        cp ${WORKDIR}/imx7d-hce-a7122-bb536.dts ${S}/arch/arm/boot/dts
        echo "dtb-\$(CONFIG_SOC_IMX7D) += imx7d-hce-a7122-bb536.dtb" >> ${S}/arch/arm/boot/dts/Makefile
}

KERNEL_DEVICETREE:append = "imx7d-hce-a7122-bb536.dtb"
DELTA_KERNEL_DEFCONFIG = "imx7-hce-a7122-bb536_defconfig"
