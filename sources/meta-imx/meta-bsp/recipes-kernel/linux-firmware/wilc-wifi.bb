LICENSE = "CLOSED"
inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "wifi.service"

SRC_URI = " file://wifi.service \
		file://start-wifi.sh \
		file://wilc1000_wifi_firmware.bin \
		file://flash_emmc.sh \
	"

do_install:append() {
	install -d ${D}/usr/bin
	install -m 0755 ${WORKDIR}/start-wifi.sh ${D}/usr/bin
	install -m 0755 ${WORKDIR}/flash_emmc.sh ${D}/usr/bin

	install -d ${D}/${systemd_unitdir}/system
	install -m 0755 ${WORKDIR}/wifi.service ${D}/${systemd_unitdir}/system
	
	install -d ${D}/lib/firmware/mchp
	install -m 0755 ${WORKDIR}/wilc1000_wifi_firmware.bin ${D}/lib/firmware/mchp
}

FILES:${PN} += "${systemd_unitdir}/system/wifi.service"
FILES:${PN} += "/usr/bin/start-wifi.sh"
FILES:${PN} += "/usr/bin/flash_emmc.sh"
FILES:${PN} += "/lib/firmware/mchp/wilc1000_wifi_firmware.bin"
