#!/bin/sh
REMOVABLE_DRIVES=""
for _device in /sys/block/*/device; do
    if echo $(readlink -f "$_device")|egrep -q "usb"; then
        _disk=$(echo "$_device" | cut -f4 -d/)
        REMOVABLE_DRIVES="$REMOVABLE_DRIVES $_disk"
    fi
done
echo Removable drives found: "${REMOVABLE_DRIVES}"
str="/dev/${REMOVABLE_DRIVES}1"
usb=$(echo $str | tr -d ' ')
echo USB found: "${usb}"

export node=/dev/mmcblk2
for((i=1; i<=16; i++)); do [ -e ${node}p${i} ] && dd if=/dev/zero of=${node}p${i} bs=1M count=1 conv=fsync; done
dd if=/dev/zero of=${node} bs=1M count=8 conv=fsync
sync
sleep 1
(echo n; echo p; echo 1; echo 8192; echo 78192; echo t; echo c; \
  echo n; echo p; echo 2; echo 78193; echo; \
  echo a; echo 1; echo p; echo w) | fdisk -u ${node}
sync
sleep 1
mkfs.vfat ${node}p1 -n BOOT
sleep 1
echo -y | mkfs.ext4 ${node}p2 -L rootfs
sleep 1

mkdir temp
mount ${usb} temp
sleep 1

dd if=temp/u-boot.imx of=${node} bs=512 seek=2 conv=fsync
sync
sleep 1

mkdir p_boot
mount -t vfat ${node}p1 p_boot
cp temp/zImage p_boot
cp temp/imx7d-hce-a7122-bb536.dtb p_boot
cp temp/imx7d-hce-a7122-bb536-imx7d-a71som.dtb p_boot
sync
sleep 1

mkdir p_rootfs
mount ${node}p2 p_rootfs
tar -xf temp/core-image-minimal-imx7d-a71som-*.rootfs.tar.gz -C p_rootfs
sync
sleep 1

echo "Flashing eMMC done... Unmounting partitions...!!!"

umount p_boot
umount p_rootfs
umount temp
rm -rf temp
rm -rf p_boot
rm -rf p_rootfs
sync

echo "DONE!!! eMMC is Ready."
