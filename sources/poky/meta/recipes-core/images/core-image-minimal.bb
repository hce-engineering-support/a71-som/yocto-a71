SUMMARY = "A small image just capable of allowing a device to boot."

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE:append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"

IMAGE_FSTYPES = "tar.gz"
IMAGE_INSTALL:append = "wilc-wifi dosfstools e2fsprogs-mke2fs \
			usbutils linux-firmware \
			libgpiod libgpiod-dev libgpiod-tools \
			i2c-tools \
			firmware-imx-sdma-imx7d \
                  	iw \
                  	wpa-supplicant \
                  	wireless-regdb-static \
                  	ethtool \
                  	dhcpcd \
			watchdog \
			"
CORE_IMAGE_EXTRA_INSTALL += " kernel-modules"
