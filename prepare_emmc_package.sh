#!/bin/sh
date=$(date +"%Y-%m-%d")
echo "Preparing required packages..."
rm -rf emmc_package
rm emmc_package_*.tar.gz
mkdir emmc_package 
cp build-a71/tmp/deploy/images/imx7d-a71som/u-boot.imx emmc_package
cp build-a71/tmp/deploy/images/imx7d-a71som/imx7d-hce-a7122-bb536.dtb emmc_package
cp build-a71/tmp/deploy/images/imx7d-a71som/imx7d-hce-a7122-bb536-imx7d-a71som.dtb emmc_package
cp build-a71/tmp/deploy/images/imx7d-a71som/zImage emmc_package
cp build-a71/tmp/deploy/images/imx7d-a71som/core-image-minimal-imx7d-a71som-*.rootfs.tar.gz emmc_package
cp flash_emmc.sh emmc_package
tar -czvf emmc_package_${date}.tar.gz emmc_package
echo "eMMC Package Ready..!!!"
